package com.epam.model.component;

public enum Topping {

    PEPPERONI,
    MUSHROOMS,
    ONIONS,
    BACON,
    EXTRA_CHEESE,
    BLACK_OLIVES,
    GREEN_PEPPERS,
    PINEAPPLE,
    SPINACH

}
