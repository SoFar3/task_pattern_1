package com.epam.model;

public class Pizza {

    private City origin;
    private PizzaType type;
    private Recipe recipe;

    public Pizza() {
    }

    public Pizza(PizzaType type, Recipe recipe) {
        this.type = type;
        this.recipe = recipe;
    }

    public Pizza(City origin, PizzaType type, Recipe recipe) {
        this.origin = origin;
        this.type = type;
        this.recipe = recipe;
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public City getOrigin() {
        return origin;
    }

    public void setOrigin(City origin) {
        this.origin = origin;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "origin=" + origin +
                ", type=" + type +
                ", recipe=" + recipe +
                '}';
    }

}
