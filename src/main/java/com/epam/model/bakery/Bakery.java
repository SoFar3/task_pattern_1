package com.epam.model.bakery;

import com.epam.model.PizzaType;
import com.epam.model.Recipe;

public interface Bakery {

    void prepare();
    void bake();
    void cut();
    void box();
    Recipe getRecipe(PizzaType type);

}
