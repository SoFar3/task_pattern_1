package com.epam.model.bakery.impl;

import com.epam.model.PizzaType;
import com.epam.model.Recipe;
import com.epam.model.bakery.Bakery;
import com.epam.model.component.Dough;
import com.epam.model.component.Sauce;
import com.epam.model.component.Topping;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LvivBakery implements Bakery {

    private Map<PizzaType, Recipe> recipes;

    public LvivBakery() {
        recipes = new HashMap<PizzaType, Recipe>();

        Recipe cheeseRecipe = new Recipe(Arrays.asList(Topping.BACON, Topping.EXTRA_CHEESE), Dough.THIN, Sauce.PESTO);
        Recipe veggieRecipe = new Recipe(Arrays.asList(Topping.BLACK_OLIVES, Topping.SPINACH), Dough.THIN, Sauce.MARINARA);
        Recipe clamRecipe = new Recipe(Collections.singletonList(Topping.PINEAPPLE), Dough.THICK, Sauce.PESTO);
        Recipe pepperoniRecipe = new Recipe(Arrays.asList(Topping.BACON, Topping.PEPPERONI), Dough.THIN, Sauce.PLUM_TOMATO);

        recipes.put(PizzaType.CHEESE, cheeseRecipe);
        recipes.put(PizzaType.VEGGIE, veggieRecipe);
        recipes.put(PizzaType.CLAM, clamRecipe);
        recipes.put(PizzaType.PEPPERONI, pepperoniRecipe);
    }

    public void prepare() {
        System.out.println("Preparing pizza...");
    }

    public void bake() {
        System.out.println("Baking pizza...");
    }

    public void cut() {
        System.out.println("Cutting pizza...");
    }

    public void box() {
        System.out.println("Boxing pizza...");
    }

    public Recipe getRecipe(PizzaType type) {
        return recipes.get(type);
    }

}
