package com.epam.model.bakery.impl;

import com.epam.model.PizzaType;
import com.epam.model.Recipe;
import com.epam.model.bakery.Bakery;
import com.epam.model.component.Dough;
import com.epam.model.component.Sauce;
import com.epam.model.component.Topping;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DniproBakery implements Bakery {

    private Map<PizzaType, Recipe> recipes;

    public DniproBakery() {
        recipes = new HashMap<PizzaType, Recipe>();

        Recipe cheeseRecipe = new Recipe(Arrays.asList(Topping.ONIONS, Topping.EXTRA_CHEESE), Dough.THICK, Sauce.PLUM_TOMATO);
        Recipe veggieRecipe = new Recipe(Arrays.asList(Topping.MUSHROOMS, Topping.GREEN_PEPPERS), Dough.THIN, Sauce.MARINARA);
        Recipe clamRecipe = new Recipe(Arrays.asList(Topping.BLACK_OLIVES, Topping.GREEN_PEPPERS), Dough.THICK, Sauce.PLUM_TOMATO);
        Recipe pepperoniRecipe = new Recipe(Arrays.asList(Topping.ONIONS, Topping.PEPPERONI), Dough.THICK, Sauce.PESTO);

        recipes.put(PizzaType.CHEESE, cheeseRecipe);
        recipes.put(PizzaType.VEGGIE, veggieRecipe);
        recipes.put(PizzaType.CLAM, clamRecipe);
        recipes.put(PizzaType.PEPPERONI, pepperoniRecipe);
    }

    public void prepare() {
        System.out.println("Preparing pizza...");
    }

    public void bake() {
        System.out.println("Baking pizza...");
    }

    public void cut() {
        System.out.println("Cutting pizza...");
    }

    public void box() {
        System.out.println("Boxing pizza...");
    }

    public Recipe getRecipe(PizzaType type) {
        return recipes.get(type);
    }

}
