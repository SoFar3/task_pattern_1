package com.epam.model;

public enum PizzaType {

    CHEESE,
    VEGGIE,
    CLAM,
    PEPPERONI

}
