package com.epam.model.factory;

import com.epam.model.City;
import com.epam.model.Pizza;
import com.epam.model.PizzaType;
import com.epam.model.bakery.impl.KyivBakery;

public class KyivPizzaFactory extends PizzaFactory {

    private static KyivPizzaFactory instance;

    private KyivPizzaFactory() {
        this.bakery = new KyivBakery();
    }

    public static KyivPizzaFactory getInstance() {
        if (instance == null) {
            instance = new KyivPizzaFactory();
        }
        return instance;
    }

    protected Pizza createPizza(PizzaType type) {
        Pizza pizza = new Pizza();
        pizza.setType(type);
        pizza.setRecipe(bakery.getRecipe(type));
        pizza.setOrigin(City.KYIV);
        return pizza;
    }

}
