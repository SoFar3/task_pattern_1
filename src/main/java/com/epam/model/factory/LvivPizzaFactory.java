package com.epam.model.factory;

import com.epam.model.City;
import com.epam.model.Pizza;
import com.epam.model.PizzaType;
import com.epam.model.bakery.impl.LvivBakery;

public class LvivPizzaFactory extends PizzaFactory {

    private static LvivPizzaFactory instance;

    private LvivPizzaFactory() {
        this.bakery = new LvivBakery();
    }

    public static LvivPizzaFactory getInstance() {
        if (instance == null) {
            instance = new LvivPizzaFactory();
        }
        return instance;
    }

    protected Pizza createPizza(PizzaType type) {
        Pizza pizza = new Pizza();
        pizza.setType(type);
        pizza.setRecipe(bakery.getRecipe(type));
        pizza.setOrigin(City.LVIV);
        return pizza;
    }

}
