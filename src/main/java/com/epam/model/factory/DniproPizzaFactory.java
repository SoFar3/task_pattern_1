package com.epam.model.factory;

import com.epam.model.City;
import com.epam.model.Pizza;
import com.epam.model.PizzaType;
import com.epam.model.bakery.impl.DniproBakery;

public class DniproPizzaFactory extends PizzaFactory {

    private static DniproPizzaFactory instance;

    private DniproPizzaFactory() {
        this.bakery = new DniproBakery();
    }

    public static DniproPizzaFactory getInstance() {
        if (instance == null) {
            instance = new DniproPizzaFactory();
        }
        return instance;
    }

    protected Pizza createPizza(PizzaType type) {
        Pizza pizza = new Pizza();
        pizza.setType(type);
        pizza.setRecipe(bakery.getRecipe(type));
        pizza.setOrigin(City.DNIPRO);
        return pizza;
    }

}
