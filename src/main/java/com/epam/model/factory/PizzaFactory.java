package com.epam.model.factory;

import com.epam.model.Pizza;
import com.epam.model.PizzaType;
import com.epam.model.bakery.Bakery;

public abstract class PizzaFactory {

    protected Bakery bakery;

    protected abstract Pizza createPizza(PizzaType type);

    public Pizza assemble(PizzaType type) {
        bakery.prepare();
        bakery.bake();
        bakery.cut();
        bakery.box();
        return createPizza(type);
    }

}
