package com.epam.controller;

import com.epam.model.PizzaType;
import com.epam.model.factory.DniproPizzaFactory;
import com.epam.model.factory.KyivPizzaFactory;
import com.epam.model.factory.LvivPizzaFactory;
import com.epam.model.factory.PizzaFactory;
import com.epam.view.Menu;
import com.epam.view.Option;
import com.epam.view.View;
import com.epam.view.ViewImpl;

public class ControllerImpl implements Controller {

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {
        Menu menu = new Menu(Integer.MAX_VALUE);

        menu.addMenuOption("1", new Option("Lviv", () -> {
            view.setMenu(pizzaTypeMenu(menu, LvivPizzaFactory.getInstance()));
        }));
        menu.addMenuOption("2", new Option("Dnipro", () -> {
            view.setMenu(pizzaTypeMenu(menu, DniproPizzaFactory.getInstance()));
        }));
        menu.addMenuOption("3", new Option("Kyiv", () -> {
            view.setMenu(pizzaTypeMenu(menu, KyivPizzaFactory.getInstance()));
        }));
        menu.addMenuOption("Q", new Option("Quit", () -> System.exit(0)));

        view.setMenu(menu);
        view.showMenu();
    }

    private Menu pizzaTypeMenu(Menu parent, PizzaFactory pizzaFactory) {
        Menu menu = new Menu(parent, 2);

        menu.addMenuOption("1", new Option(PizzaType.CHEESE.name(), () -> {
            System.out.println(pizzaFactory.assemble(PizzaType.CHEESE));
        }));
        menu.addMenuOption("2", new Option(PizzaType.PEPPERONI.name(), () -> {
            System.out.println(pizzaFactory.assemble(PizzaType.PEPPERONI));
        }));
        menu.addMenuOption("3", new Option(PizzaType.VEGGIE.name(), () -> {
            System.out.println(pizzaFactory.assemble(PizzaType.VEGGIE));
        }));
        menu.addMenuOption("4", new Option(PizzaType.CLAM.name(), () -> {
            System.out.println(pizzaFactory.assemble(PizzaType.CLAM));
        }));
        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));

        return menu;
    }

}
